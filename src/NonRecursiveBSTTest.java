/**
 * Created by Brendan Burgess on 10/25/2015.
 */
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import static org.junit.Assert.*;

public class NonRecursiveBSTTest {

    @org.junit.Before
    public void setUp() throws Exception {
    BST recursiveBST = new BST();
    NonRecursiveBST nonRecursiveBST = new NonRecursiveBST();

    }

    @org.junit.Test
    public void testGet(){
        NonRecursiveBST<Integer, Integer> nonRBST = new NonRecursiveBST<Integer, Integer>();

        nonRBST.put(1, 1);
        nonRBST.put(2, 2);
        nonRBST.put(3, 3);
        nonRBST.put(4, 4);
        nonRBST.put(5, 5);
        nonRBST.put(6, 6);
        nonRBST.put(7, 7);
        nonRBST.put(8, 8);
        
        BST<Integer, Integer> bst = new BST<Integer, Integer>();
        
        bst.put(1, 1);
        bst.put(2, 2);
        bst.put(3, 3);
        bst.put(4, 4);
        bst.put(5, 5);
        bst.put(6, 6);
        bst.put(7, 7);
        bst.put(8, 8);
        
        for (int i = 1; i < 9; i++) {
            assertEquals(nonRBST.get(i), bst.get(i));
        }

        assertEquals(nonRBST.get(30), null);


        
    }

    @org.junit.Test
    public void testPut(){
        NonRecursiveBST<String, Integer> nonRBST = new NonRecursiveBST<String, Integer>();
        nonRBST.put("S", 1);
        nonRBST.put("X", 2);
        nonRBST.put("E", 3);
        nonRBST.put("R", 4);
        nonRBST.put("A", 5);
        nonRBST.put("H", 6);
        nonRBST.put("C", 7);
        nonRBST.put("M", 8);

        assertEquals(true, nonRBST.isBST());
        assertEquals(8, nonRBST.size());

    }
@org.junit.Test
    public void testMin(){
        BST<Integer, Integer> st = new BST<Integer, Integer>();
        NonRecursiveBST<Integer, Integer> nonRBST = new NonRecursiveBST<Integer, Integer>();
        int[] input = {5 , 40, 912, 432, 883, 0, 80, -100, 46, -3, 10};
        for (int i = 0; i < input.length; i++) {
            Integer key = input[i];
            st.put(key, i);
            nonRBST.put(key, i);
        }
        assertTrue(st.min() == nonRBST.min());
        assertTrue(st.min() == nonRBST.min());
       
        
        

    }

    @org.junit.Test
    public void testMax(){
        BST<Integer, Integer> st = new BST<Integer, Integer>();
        NonRecursiveBST<Integer, Integer> nonRBST = new NonRecursiveBST<Integer, Integer>();
        int[] input = {5 , 40, 912, 432, 883, 0, 80, -100, 46, -3, 10};
        for (int i = 0; i < input.length; i++) {
            Integer key = input[i];
            st.put(key, i);
            nonRBST.put(key, i);
        }
        assertTrue(st.min() == nonRBST.min());
        assertTrue(st.min() == nonRBST.min());
        assertTrue(st.min() == nonRBST.min());

    }

    @Test
    public final void testFloor1() {
        BST<Integer, Integer> st = new BST<Integer, Integer>();
        int[] input = { 70, 60, 50, 80, 119, 90, 65, 75, 99, 101 };
        for (int i = 0; i < input.length; i++) {
            Integer key = (Integer) input[i];
            st.put(key, i);
        }
        assertEquals(99, (int) st.floor(100));
        // corner case 1: key is exactly the current node
        assertEquals(101, (int) st.floor(101));

        // corner case 2: key is larger than the largest one in BST
        assertEquals(119, (int) st.floor(120));
    }

    @Test
    public final void testFloor2() {
        BST<Character, Integer> st = new BST<Character, Integer>();
        char[] input = { 'H', 'C', 'B', 'S', 'R', 'X', 'E' };
        for (int i = 0; i < input.length; i++) {
            Character key = (Character) input[i];
            st.put(key, i);
        }
        assertEquals('S', (char) st.floor('T'));
        assertEquals('X', (char) st.floor('Z'));
        assertEquals('H', (char) st.floor('J'));
        assertEquals('B', (char) st.floor('B'));
        assertEquals('R', (char) st.floor('R'));

        try  {
            st.floor(null);
        }
         catch (NullPointerException error) {
            //Pass if exception caught
        }

        assertNull( st.floor('A'));
    }

    @Test
    public final void testCeiling1() {
        BST<Character, Integer> st = new BST<Character, Integer>();
        char[] input = { 'H', 'C', 'A', 'S', 'R', 'X', 'E' };
        for (int i = 0; i < input.length; i++) {
            Character key = (Character) input[i];
            st.put(key, i);
        }
        assertEquals('X', (char) st.ceiling('T'));
    }

    @Test
    public final void testCeiling2() {
        BST<Integer, Integer> st = new BST<Integer, Integer>();
        int[] input = { 70, 60, 50, 80, 120, 90, 65, 75, 99, 101 };
        for (int i = 0; i < input.length; i++) {
            Integer key = (Integer) input[i];
            st.put(key, i);
        }
        assertEquals(101, (int) st.ceiling(100));
    }

    @org.junit.Test
    public void testSelect(){
    NonRecursiveBST<String, Integer> nonRBST = new NonRecursiveBST<String, Integer>();
    
    //Empty tree test
    try {
      nonRBST.select(6);
    } catch (IllegalArgumentException error) {
      //Pass if exception caught   
    }
    
    nonRBST.put("S", 1);
    
    // One element BST
    String testValue = nonRBST.select(0);
    assertTrue(testValue.compareTo("S") == 0); 
    
    nonRBST.put("X", 2);
    nonRBST.put("E", 3);
    nonRBST.put("R", 4);
    nonRBST.put("A", 5);
    nonRBST.put("H", 6);
    nonRBST.put("C", 7);
    nonRBST.put("M", 8);
    
    //Negative parameter passed
    try {
      nonRBST.select(-6);
    } catch (IllegalArgumentException error) { 
      //Pass if exception caught
    }
    
    //Size of nonRecursiveBST as parameter
    try {
      nonRBST.select(8);
    } catch (IllegalArgumentException error) { 
      //Pass if exception caught
    }
    
    // Tests all values within nonRecursiveBST
    testValue = nonRBST.select(0);
    assertTrue(testValue.compareTo("A") == 0);
    
    testValue = (String)nonRBST.select(1);
    assertTrue(testValue.compareTo("C") == 0);
    
    testValue = (String)nonRBST.select(2);
    assertTrue(testValue.compareTo("E") == 0);
    
    testValue = (String)nonRBST.select(3);
    assertTrue(testValue.compareTo("H") == 0);
    
    testValue = (String)nonRBST.select(4);
    assertTrue(testValue.compareTo("M") == 0);
    
    testValue = (String)nonRBST.select(5);
    assertTrue(testValue.compareTo("R") == 0);
    
    testValue = (String)nonRBST.select(6);
    assertTrue(testValue.compareTo("S") == 0);
    
    testValue = (String)nonRBST.select(7);
    assertTrue(testValue.compareTo("X") == 0);
    
    }// End of testSelect()

    @org.junit.Test
    public void testRank(){
        NonRecursiveBST<Integer, Integer> nonRBST = new NonRecursiveBST<Integer, Integer>();

        nonRBST.put(1, 1);
        nonRBST.put(2, 2);
        nonRBST.put(3, 3);
        nonRBST.put(4, 4);
        nonRBST.put(5, 5);
        nonRBST.put(6, 6);
        nonRBST.put(7, 7);
        nonRBST.put(8, 8);
        
        BST<Integer, Integer> bst = new BST<Integer, Integer>();
        
        bst.put(1, 1);
        bst.put(2, 2);
        bst.put(3, 3);
        bst.put(4, 4);
        bst.put(5, 5);
        bst.put(6, 6);
        bst.put(7, 7);
        bst.put(8, 8);
        
        for (int i = 1; i < 9; i++) {
            assertEquals(nonRBST.rank(i), bst.rank(i));
        }
    }

    @org.junit.Test
    public void testKeys(){

        NonRecursiveBST<Integer, Integer> nonRBST = new NonRecursiveBST<Integer, Integer>();

        nonRBST.put(1, 1);
        nonRBST.put(2, 2);
        nonRBST.put(3, 3);
        nonRBST.put(4, 4);
        nonRBST.put(5, 5);
        nonRBST.put(6, 6);
        nonRBST.put(7, 7);
        nonRBST.put(8, 8);


        Iterable<Integer> its = nonRBST.keys();
        int counter = 1;

        for(int s : its){
            assertEquals(counter, s);
            counter++;
        }

        NonRecursiveBST<String, Integer> nonRBST2 = new NonRecursiveBST<String, Integer>();
        nonRBST2.put("S", 1);
        nonRBST2.put("X", 2);
        nonRBST2.put("E", 3);
        nonRBST2.put("R", 4);
        nonRBST2.put("A", 5);
        nonRBST2.put("H", 6);
        nonRBST2.put("C", 7);
        nonRBST2.put("M", 8);

        BST<String, Integer> myBST = new BST<>();
        myBST.put("S", 1);
        myBST.put("X", 2);
        myBST.put("E", 3);
        myBST.put("R", 4);
        myBST.put("A", 5);
        myBST.put("H", 6);
        myBST.put("C", 7);
        myBST.put("M", 8);

        Iterable<String>  stringReturns1 = nonRBST2.keys();
        Iterable<String>  stringReturns2 = nonRBST2.keys();

        String[] currents = new String[8];
        counter = 0;

        for(String s : stringReturns1){
            currents[counter] = s;
            counter++;
        }

        counter = 0;
        for(String s : stringReturns2){
            assertEquals(s, currents[counter]);
            counter++;
        }


    }

    /**
     * Testing client for other NonRecursiveBST
     * 
     * Example Output: 
     * 
     * (Failed case)
     * NonRecursiveBST did not pass all tests
     * testGet: null
     * Failed: 1/11 tests.
     * 
     * (Pass case) 
     * NonRecursiveBST passed all tests.
     * 
     */
    public static void main(String[] args) {
      Result tests = new Result(); // Holds all the testing information
      JUnitCore runner = new JUnitCore(); // Runs all of the test methods we defined
      tests = runner.run(NonRecursiveBSTTest.class); 
      int numFailed = tests.getFailureCount(); 
      if( numFailed < 1) {
        System.out.println("NonRecursiveBST passed all tests.");
        return;
      }
      // Some tests failed
      System.out.println("NonRecursiveBST did not pass all tests");
      for(Failure failed : tests.getFailures()) {
        System.out.print(failed.getDescription().getMethodName() + ": "); // method name
        System.out.println(failed.getMessage()); // failed message
      }
      System.out.println("Failed: " + numFailed + "/" + tests.getRunCount() + " tests.");
    }
}