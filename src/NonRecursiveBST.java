/**
 * Created by Brendan Burgess on 10/24/2015.
 */

import java.util.*;
import java.util.Stack;

/**
 *  The <tt>BST</tt> class represents an ordered symbol table of generic
 *  key-value pairs.
 *  It supports the usual <em>put</em>, <em>get</em>, <em>contains</em>,
 *  <em>delete</em>, <em>size</em>, and <em>is-empty</em> methods.
 *  It also provides ordered methods for finding the <em>minimum</em>,
 *  <em>maximum</em>, <em>floor</em>, <em>select</em>, <em>ceiling</em>.
 *  It also provides a <em>keys</em> method for iterating over all of the keys.
 *  A symbol table implements the <em>associative array</em> abstraction:
 *  when associating a value with a key that is already in the symbol table,
 *  the convention is to replace the old value with the new value.
 *  Unlike {@link java.util.Map}, this class uses the convention that
 *  values cannot be <tt>null</tt>&mdash;setting the
 *  value associated with a key to <tt>null</tt> is equivalent to deleting the key
 *  from the symbol table.
 *  <p>
 *  This implementation uses an (unbalanced) binary search tree. It requires that
 *  the key type implements the <tt>Comparable</tt> interface and calls the
 *  <tt>compareTo()</tt> and method to compare two keys. It does not call either
 *  <tt>equals()</tt> or <tt>hashCode()</tt>.
 *  The <em>put</em>, <em>contains</em>, <em>remove</em>, <em>minimum</em>,
 *  <em>maximum</em>, <em>ceiling</em>, <em>floor</em>, <em>select</em>, and
 *  <em>rank</em>  operations each take
 *  linear time in the worst case, if the tree becomes unbalanced.
 *  The <em>size</em>, and <em>is-empty</em> operations take constant time.
 *  Construction takes constant time.
 *  <p>
 *  For additional documentation, see <a href="http://algs4.cs.princeton.edu/32bst">Section 3.2</a> of
 *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 *  For other implementations, see {@link ST}, {@link BinarySearchST},
 *  {@link SequentialSearchST}, {@link RedBlackBST},
 *  {@link SeparateChainingHashST}, and {@link LinearProbingHashST},
 *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 */
public class NonRecursiveBST <Key extends Comparable<Key>, Value> extends BaseBST<Key, Value> {

    public NonRecursiveBST(){

    }

    /**
     * Returns the value associated with the given key.
     *
     * @param  key the key
     * @return the value associated with the given key if the key is in the symbol table
     *         and <tt>null</tt> if the key is not in the symbol table
     * @throws NullPointerException if <tt>key</tt> is <tt>null</tt>
     */
   public Value get(Key key) {
        if(key == null){
            throw new NullPointerException();
        }
    	Node current = root;
        while(current != null){
            int compare = key.compareTo(current.key);
            if(compare < 0 ){
                current = current.left;
            }
            if(compare > 0 ){
                current = current.right;
            } else {
                return current.val;
            }
        }
		return null;
    }


    /**
     * Inserts the key-value pair into the symbol table, overwriting the old value
     * with the new value if the key is already in the symbol table.
     * If the value is <tt>null</tt>, this effectively deletes the key from the symbol table.
     *
     * @param  key the key
     * @param  val the value
     * @throws NullPointerException if <tt>key</tt> is <tt>null</tt>
     */
     /**
     * Nonrecursive implementation of put.
     * Add itemes to the BST
     * 
     * The only way i could think of doing it was with a 
     * sloper verson of what Prinston gave us.
     *        
     */
    public void put(Key key, Value val) {
        //if key is null
        if(key == null){
            throw new NullPointerException();
        }

        //starts new tree if necessary
        Node NewNode = new Node(key, val, 1); //creates a new node
        if (root == null) {					// if there is no root it makes one
            root = NewNode;
            return;
        }


        Node parent = null, current = root; // creates two nodes parent and current
        while (current != null) {			// while current is not null, parent is set to equel current
            current.N++;
            parent = current;
            int res = key.compareTo(current.key); //if the new Value is more then the current                   
            if      (res < 0) current = current.left;//Nodes values the search goes to the right on the tree,
            else if (res > 0) current = current.right;//if not it goes to the left
            else {
            	current.val = val;
                break;
            }   // overwrite duplicate
        }
        int res = key.compareTo(parent.key);	// after finding the last parent where the new valus shoud be attached
        if (res < 0) parent.left  = NewNode;	// we check to see wich side of the parent to add the name to, and then add it
        else         parent.right = NewNode;

    }

    /**
     * Returns the smallest key in the symbol table.
     *
     * @return the smallest key in the symbol table
     * @throws NoSuchElementException if the symbol table is empty
     */
    public Key min(){
        if (isEmpty())
			throw new NoSuchElementException(
					"called min() with empty symbol table");
		Node x = root;
		if (x.left == null) {
			return x.key;
		}
		while (x.left != null) {
			x = x.left;
		}

		return x.key;
    }

    /**
     * Returns the largest key in the symbol table.
     *
     * @return the largest key in the symbol table
     * @throws NoSuchElementException if the symbol table is empty
     */
    public Key max(){
        if (isEmpty())
			throw new NoSuchElementException(
					"called max() with empty symbol table");
		Node x = root;
		if (x.right == null) {
			return x.key;
		}

		while (x.right != null) {
			x = x.right;
		}

		return x.key;
    }

    /**
     * Returns the largest key in the symbol table less than or equal to <tt>key</tt>.
     *
     * @param  key the key
     * @return the largest key in the symbol table less than or equal to <tt>key</tt>
     * @throws NoSuchElementException if there is no such key
     * @throws NullPointerException if <tt>key</tt> is <tt>null</tt>
     */
    public Key floor(Key key) {
        if (key == null)
            throw new NullPointerException("called floor() with null key");

        if (isEmpty())
            throw new NoSuchElementException(
                    "called floor() with empty symbol table");

        Node x = floor(root, key);
        if (x == null)
            return null;
        else
            return x.key;
    }

    private Node floor(Node x, Key key) {
        if (key.compareTo(x.key) < 0 && x.left.key == null)
            return null;
        if (x.key == key)
            return x;

        else if (key.compareTo(x.key) > 0) {
            while (key.compareTo(x.right.key) > 0 && x.right != null)
                x = x.right;
        } else {
            while (key.compareTo(x.key) < 0 && x.left != null)
                x = x.left;
            while (key.compareTo(x.right.key) >= 0 && x.right != null)
                x = x.right;
        }
        return x;
    }

    /**
     * Returns the smallest key in the symbol table greater than or equal to <tt>key</tt>.
     *
     * @param  key the key
     * @return the smallest key in the symbol table greater than or equal to <tt>key</tt>
     * @throws NoSuchElementException if there is no such key
     * @throws NullPointerException if <tt>key</tt> is <tt>null</tt>
     */
    public Key ceiling(Key key) {
        if (isEmpty())
            throw new NoSuchElementException(
                    "called ceiling() with empty symbol table");
        if (key == null)
            throw new NullPointerException("called ceiling() with null key");

        Node x = ceiling(root, key);
        if (x == null)
            return null;
        else
            return x.key;
    }

    private Node ceiling(Node x, Key key) {
        if (key.compareTo(x.key) > 0 && x.right.key == null)
            return null;
        if (key == x.key)
            return x;

        else if (key.compareTo(x.key) < 0) {
            while (x.left.key.compareTo(key) > 0 && x.left != null)
                x = x.left;
        } else {
            while (key.compareTo(x.key) > 0 && x.right != null)
                x = x.right;

            while (x.left.key.compareTo(key) >= 0 && x.left != null)
                x = x.left;
        }
        return x;
    }

    /**
     * Return the kth smallest key in the symbol table.
     *
     * @param  k the order statistic
     * @return the kth smallest key in the symbol table
     * @throws IllegalArgumentException unless <tt>k</tt> is between 0 and
     *        <em>N</em> &minus; 1
     */
    public Key select(int k){
      if (k < 0 || k >= size()) throw new IllegalArgumentException();
      Node temp = root; 
      int currentK = k;
      while(temp != null) {
        int t;
        if(temp.left != null) {
          t = temp.left.N;
        } else {
         t = 0;
        }
        if(t > currentK) {
          temp = temp.left;//Look in the left subtree if t > current K
        }
        else if(t < currentK) {
          temp = temp.right; //Look in the right subtree if t < current K
          currentK = currentK-t-1;
        } else {
          return temp.key;
        }
      } // key not found at rank k
      return null;
    }

    /**
     * Return the number of keys in the symbol table strictly less than <tt>key</tt>.
     *
     * @param  key the key
     * @return the number of keys in the symbol table strictly less than <tt>key</tt>
     * @throws NullPointerException if <tt>key</tt> is <tt>null</tt>
     */
    public int rank(Key key){
        //excepton for null value
        if(key == null){
            throw new NullPointerException();
        }

        int count = 0;
        Node x = root;
        while (x != null) {
            int cmp = (key.compareTo(x.key));
            if (cmp < 0) {
                x = x.left;
            } else if (cmp > 0 ) {
                if (x.left != null) {
                    count += 1 + x.left.N;
                } else {
                    count++;
                }
                x = x.right;
            } else {
                if(x.left != null){
                    count += x.left.N;
                }
                return count;
            }
        }
        return count;
    }

    /**
     * Returns all keys in the symbol table as an <tt>Iterable</tt>.
     * To iterate over all of the keys in the symbol table named <tt>st</tt>,
     * use the foreach notation: <tt>for (Key key : st.keys())</tt>.
     *
     * @return all keys in the symbol table
     */

    public Iterable<Key> keys(){
        if (isEmpty() ){
            throw new IllegalArgumentException();
        }

        Stack<Node> nodeStack = new Stack<>();
        Stack<Key> inOrder = new Stack<>();
        Node current = root;
        while(!nodeStack.isEmpty() || current != null){
            if( current != null ){
                nodeStack.push(current);
                current = current.left;
            } else {
                current = nodeStack.pop();
                inOrder.push(current.key);
                current = current.right;
            }
        }
        return inOrder;
    }
}
